var map = L.map('main_map').setView([51.505, -0.09, 13]);

/* L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy: <a href="https://www.openstreetmap.org/copyright">OpenStreetMap>/a> contributors'
}).addTo(map); */

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

// L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
// maxZoom: 18
// }).addTo(map);

// L.maker([-34.6012424,-58.3861497]);
// L.maker([-34.596932,-58.3808287]);
// L.maker([-34.599584,-58.3778777]);

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.maker(bici.ubicacion, {title:bici.id}).addTo(map);
        })
    }
})