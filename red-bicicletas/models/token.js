const mongoose = require('mongoose')

const Schema = mongoose.Schema;
const TokenSchema = new Schema({
    _userId: { type: mongoose.Schema.Types.ObjectId, required: true, refs: 'Usuario' },
    token: { type: String, required: true},
    createdAt: {type: Date, required: true, default: Date.now, expires: 43200}
});

module.exports = mongoose.model('AuthToken', TokenSchema);