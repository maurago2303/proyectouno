var express = require('express')
var router = express.Router();
var auth = require('../../controllers/api/authController')

router.post('/authenticate', auth.authenticate);
router.post('/forgotPassword', auth.forgotPassword);

module.exports = router;